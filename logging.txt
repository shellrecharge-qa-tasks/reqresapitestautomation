
******************************************************
***   Reqres Request Sent at: 25/07/2022 09:57:25   ***
******************************************************

Request method:	GET
Request URI:	https://reqres.in/api/users?page=1
Proxy:			<none>
Request params:	<none>
Query params:	<none>
Form params:	<none>
Path params:	<none>
Headers:		Accept=*/*
				Content-Type=application/json; charset=UTF-8
Cookies:		<none>
Multiparts:		<none>
Body:			<none>
HTTP/1.1 200 OK
Date: Mon, 25 Jul 2022 06:57:37 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 996
Connection: keep-alive
X-Powered-By: Express
Access-Control-Allow-Origin: *
Etag: W/"3e4-2RLXvr5wTg9YQ6aH95CkYoFNuO8"
Via: 1.1 vegur
Cache-Control: max-age=14400
CF-Cache-Status: HIT
Age: 372
Accept-Ranges: bytes
Expect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"
Report-To: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v3?s=%2B56at8zbF%2FzkGMFsu0d01PVXKayZ00Yzc3jdmlbxwpHixIammjFbdsdTx4mh8jlIiZjuYXySlmTWbi3p2tBC5oPK0Q2ksIQGwlRN1cEce02QSPw2nr9p1qxFNg%3D%3D"}],"group":"cf-nel","max_age":604800}
NEL: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
Vary: Accept-Encoding
Server: cloudflare
CF-RAY: 730300a46c9692db-IST

{
    "page": 1,
    "per_page": 6,
    "total": 12,
    "total_pages": 2,
    "data": [
        {
            "id": 1,
            "email": "george.bluth@reqres.in",
            "first_name": "George",
            "last_name": "Bluth",
            "avatar": "https://reqres.in/img/faces/1-image.jpg"
        },
        {
            "id": 2,
            "email": "janet.weaver@reqres.in",
            "first_name": "Janet",
            "last_name": "Weaver",
            "avatar": "https://reqres.in/img/faces/2-image.jpg"
        },
        {
            "id": 3,
            "email": "emma.wong@reqres.in",
            "first_name": "Emma",
            "last_name": "Wong",
            "avatar": "https://reqres.in/img/faces/3-image.jpg"
        },
        {
            "id": 4,
            "email": "eve.holt@reqres.in",
            "first_name": "Eve",
            "last_name": "Holt",
            "avatar": "https://reqres.in/img/faces/4-image.jpg"
        },
        {
            "id": 5,
            "email": "charles.morris@reqres.in",
            "first_name": "Charles",
            "last_name": "Morris",
            "avatar": "https://reqres.in/img/faces/5-image.jpg"
        },
        {
            "id": 6,
            "email": "tracey.ramos@reqres.in",
            "first_name": "Tracey",
            "last_name": "Ramos",
            "avatar": "https://reqres.in/img/faces/6-image.jpg"
        }
    ],
    "support": {
        "url": "https://reqres.in/#support-heading",
        "text": "To keep ReqRes free, contributions towards server costs are appreciated!"
    }
}

******************************************************
***   Reqres Request Sent at: 25/07/2022 09:57:27   ***
******************************************************

Request method:	GET
Request URI:	https://reqres.in/api/users?page=1
Proxy:			<none>
Request params:	<none>
Query params:	<none>
Form params:	<none>
Path params:	<none>
Headers:		Accept=*/*
				Content-Type=application/json; charset=UTF-8
Cookies:		<none>
Multiparts:		<none>
Body:			<none>
HTTP/1.1 200 OK
Date: Mon, 25 Jul 2022 06:57:39 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 996
Connection: keep-alive
X-Powered-By: Express
Access-Control-Allow-Origin: *
Etag: W/"3e4-2RLXvr5wTg9YQ6aH95CkYoFNuO8"
Via: 1.1 vegur
Cache-Control: max-age=14400
CF-Cache-Status: HIT
Age: 374
Accept-Ranges: bytes
Expect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"
Report-To: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v3?s=Wf9%2FEl2ShvAE3jKkt3AR%2FsIrS6N%2FlZudmhE%2Bwqtwy%2BkJ7eRyXr9Sfz6XfgFHibXp4KAnxqB0GU%2FX7shdtrUlPbk3uTiWX42I%2F8pXJtPFkQytNSRwNl70Q1UR8Q%3D%3D"}],"group":"cf-nel","max_age":604800}
NEL: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
Vary: Accept-Encoding
Server: cloudflare
CF-RAY: 730300adcf835493-IST

{
    "page": 1,
    "per_page": 6,
    "total": 12,
    "total_pages": 2,
    "data": [
        {
            "id": 1,
            "email": "george.bluth@reqres.in",
            "first_name": "George",
            "last_name": "Bluth",
            "avatar": "https://reqres.in/img/faces/1-image.jpg"
        },
        {
            "id": 2,
            "email": "janet.weaver@reqres.in",
            "first_name": "Janet",
            "last_name": "Weaver",
            "avatar": "https://reqres.in/img/faces/2-image.jpg"
        },
        {
            "id": 3,
            "email": "emma.wong@reqres.in",
            "first_name": "Emma",
            "last_name": "Wong",
            "avatar": "https://reqres.in/img/faces/3-image.jpg"
        },
        {
            "id": 4,
            "email": "eve.holt@reqres.in",
            "first_name": "Eve",
            "last_name": "Holt",
            "avatar": "https://reqres.in/img/faces/4-image.jpg"
        },
        {
            "id": 5,
            "email": "charles.morris@reqres.in",
            "first_name": "Charles",
            "last_name": "Morris",
            "avatar": "https://reqres.in/img/faces/5-image.jpg"
        },
        {
            "id": 6,
            "email": "tracey.ramos@reqres.in",
            "first_name": "Tracey",
            "last_name": "Ramos",
            "avatar": "https://reqres.in/img/faces/6-image.jpg"
        }
    ],
    "support": {
        "url": "https://reqres.in/#support-heading",
        "text": "To keep ReqRes free, contributions towards server costs are appreciated!"
    }
}

******************************************************
***   Reqres Request Sent at: 25/07/2022 09:57:28   ***
******************************************************

Request method:	GET
Request URI:	https://reqres.in/api/users/1
Proxy:			<none>
Request params:	<none>
Query params:	<none>
Form params:	<none>
Path params:	id=1
Headers:		Accept=*/*
				Content-Type=application/json; charset=UTF-8
Cookies:		<none>
Multiparts:		<none>
Body:			<none>
HTTP/1.1 200 OK
Date: Mon, 25 Jul 2022 06:57:39 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 280
Connection: keep-alive
X-Powered-By: Express
Access-Control-Allow-Origin: *
Etag: W/"118-P3iKcVpIgCj9HqYeOOrGLX8qJVc"
Via: 1.1 vegur
Cache-Control: max-age=14400
CF-Cache-Status: HIT
Age: 372
Accept-Ranges: bytes
Expect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"
Report-To: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v3?s=XJzFQRkpGaGE42RwEQ2vVJOMyj7fu9B2dcEtXQVAVn3XSIc3nT6zaA1AErbqL77kw84JwO9DNSlxlnpbMQHWJgZ5t6RCCvxK6pi1FpsFnkZvlO7U0m7Pbhfhzg%3D%3D"}],"group":"cf-nel","max_age":604800}
NEL: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
Vary: Accept-Encoding
Server: cloudflare
CF-RAY: 730300af39ad92d7-IST

{
    "data": {
        "id": 1,
        "email": "george.bluth@reqres.in",
        "first_name": "George",
        "last_name": "Bluth",
        "avatar": "https://reqres.in/img/faces/1-image.jpg"
    },
    "support": {
        "url": "https://reqres.in/#support-heading",
        "text": "To keep ReqRes free, contributions towards server costs are appreciated!"
    }
}

******************************************************
***   Reqres Request Sent at: 25/07/2022 09:57:28   ***
******************************************************

Request method:	POST
Request URI:	https://reqres.in/api/users
Proxy:			<none>
Request params:	<none>
Query params:	<none>
Form params:	<none>
Path params:	<none>
Headers:		Accept=*/*
				Content-Type=application/json; charset=UTF-8
Cookies:		<none>
Multiparts:		<none>
Body:
{
    "name": "Marc",
    "job": "Software Developer"
}
HTTP/1.1 201 Created
Date: Mon, 25 Jul 2022 06:57:40 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 92
Connection: keep-alive
X-Powered-By: Express
Access-Control-Allow-Origin: *
Etag: W/"5c-FhkBR6CjU67duOz0n1jzMzlD87c"
Via: 1.1 vegur
CF-Cache-Status: DYNAMIC
Expect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"
Report-To: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v3?s=04gpbQmNNf9GS5T8Lr3TcWQN1RqZTqHTjm1uM3nuA1%2BSazNKp%2BxKcBOsXMymfNH%2BtDZ47vOfLvlsjRkmM6s07jfklQUQfvPmRv8ql6yZZulXzxXF7nJs%2FirazQ%3D%3D"}],"group":"cf-nel","max_age":604800}
NEL: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
Server: cloudflare
CF-RAY: 730300b3292c92dd-IST

{
    "name": "Marc",
    "job": "Software Developer",
    "id": "479",
    "createdAt": "2022-07-25T06:57:40.445Z"
}

******************************************************
***   Reqres Request Sent at: 25/07/2022 09:57:29   ***
******************************************************

Request method:	POST
Request URI:	https://reqres.in/api/users
Proxy:			<none>
Request params:	<none>
Query params:	<none>
Form params:	<none>
Path params:	<none>
Headers:		Accept=*/*
				Content-Type=application/json; charset=UTF-8
Cookies:		<none>
Multiparts:		<none>
Body:
{
    "name": "Ali",
    "job": "QA"
}
HTTP/1.1 201 Created
Date: Mon, 25 Jul 2022 06:57:41 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 75
Connection: keep-alive
X-Powered-By: Express
Access-Control-Allow-Origin: *
Etag: W/"4b-uuw236DpxepDICxB9A5vf6IxL5g"
Via: 1.1 vegur
CF-Cache-Status: DYNAMIC
Expect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"
Report-To: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v3?s=z36lt0f%2FY52PZErM%2B6D8B99qLL7NJVKX5M9LfZ887NrRbU0Y6lQ8gXOK9Mgq1tH8doF2Z%2FRcyO0mpzS%2BNPF%2BnT%2F6XknOX3YswO0sgFq18Xl%2FXxCT92CLROfZaQ%3D%3D"}],"group":"cf-nel","max_age":604800}
NEL: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
Server: cloudflare
CF-RAY: 730300b5ef8292d7-IST

{
    "name": "Ali",
    "job": "QA",
    "id": "746",
    "createdAt": "2022-07-25T06:57:40.947Z"
}

******************************************************
***   Reqres Request Sent at: 25/07/2022 09:57:29   ***
******************************************************

Request method:	PUT
Request URI:	https://reqres.in/api/users/746
Proxy:			<none>
Request params:	<none>
Query params:	<none>
Form params:	<none>
Path params:	id=746
Headers:		Accept=*/*
				Content-Type=application/json; charset=UTF-8
Cookies:		<none>
Multiparts:		<none>
Body:
{
    "name": "Steve",
    "job": "Lawyer"
}
HTTP/1.1 200 OK
Date: Mon, 25 Jul 2022 06:57:41 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 70
Connection: keep-alive
X-Powered-By: Express
Access-Control-Allow-Origin: *
Etag: W/"46-y6L5yKbcZXLj2AkLEo4JmB36Xz4"
Via: 1.1 vegur
CF-Cache-Status: DYNAMIC
Expect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"
Report-To: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v3?s=qm2xEJR0heBt4CkOPRdP1c%2BlQlbREy42UVp4aSGuUg0Yau9I7WpmgnPbnJ2e%2B58valw5hNWpu7vtXDYtX5YJPLu7ARXf3rS6VnCr8NCBVAUqARksi80nWwu7sg%3D%3D"}],"group":"cf-nel","max_age":604800}
NEL: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
Vary: Accept-Encoding
Server: cloudflare
CF-RAY: 730300b7ff4c92cf-IST

{
    "name": "Steve",
    "job": "Lawyer",
    "updatedAt": "2022-07-25T06:57:41.212Z"
}

******************************************************
***   Reqres Request Sent at: 25/07/2022 09:57:30   ***
******************************************************

Request method:	POST
Request URI:	https://reqres.in/api/users
Proxy:			<none>
Request params:	<none>
Query params:	<none>
Form params:	<none>
Path params:	<none>
Headers:		Accept=*/*
				Content-Type=application/json; charset=UTF-8
Cookies:		<none>
Multiparts:		<none>
Body:
{
    "name": "Ali",
    "job": "QA"
}
HTTP/1.1 201 Created
Date: Mon, 25 Jul 2022 06:57:41 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 75
Connection: keep-alive
X-Powered-By: Express
Access-Control-Allow-Origin: *
Etag: W/"4b-bJIU+/weguh9i0Ng9WAv0VX+a/g"
Via: 1.1 vegur
CF-Cache-Status: DYNAMIC
Expect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"
Report-To: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v3?s=ve7y6hi4x%2BTz9M4QWxYvIINFEEnp1ZO3UNWyMJY6NA2S1WYHKbsRvDD5%2BSsKU10sjV9GpNvQOvPzDPC5OtH14sEf6eZsXndsJAOAfYLnnsBCfyu2CmeNfLwIKA%3D%3D"}],"group":"cf-nel","max_age":604800}
NEL: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
Server: cloudflare
CF-RAY: 730300ba3f9592d2-IST

{
    "name": "Ali",
    "job": "QA",
    "id": "811",
    "createdAt": "2022-07-25T06:57:41.580Z"
}

******************************************************
***   Reqres Request Sent at: 25/07/2022 09:57:30   ***
******************************************************

Request method:	PATCH
Request URI:	https://reqres.in/api/users/811
Proxy:			<none>
Request params:	<none>
Query params:	<none>
Form params:	<none>
Path params:	id=811
Headers:		Accept=*/*
				Content-Type=application/json; charset=UTF-8
Cookies:		<none>
Multiparts:		<none>
Body:
{
    "name": "Georgia",
    "job": "Doctor"
}
HTTP/1.1 200 OK
Date: Mon, 25 Jul 2022 06:57:41 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 72
Connection: keep-alive
X-Powered-By: Express
Access-Control-Allow-Origin: *
Etag: W/"48-G+4UV4dmg+y/iXYGqWfM2cOCgx4"
Via: 1.1 vegur
CF-Cache-Status: DYNAMIC
Expect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"
Report-To: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v3?s=Fz0tvwH9ccTTscSiI7hDKL8eH1HUnrUZmlPAbJMMDQ1ZMDYjbCylgkkS2hsH7Fygv9EhroEy4CqSP5SIFDXo3yXeoiVpJgQdhnyTnbas7ErLUcMAEgjmO6Zmlg%3D%3D"}],"group":"cf-nel","max_age":604800}
NEL: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
Server: cloudflare
CF-RAY: 730300bc2ea692d1-IST

{
    "name": "Georgia",
    "job": "Doctor",
    "updatedAt": "2022-07-25T06:57:41.914Z"
}
